package com.example.prctica1c2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class list_activity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<com.example.prctica1c2.Contacto> contactos;
    private ArrayList<com.example.prctica1c2.Contacto> filtro;
    private int indx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");

        indx = (int) bundleObject.getInt("index");
        filtro = contactos;

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("listaContactos",filtro);
                bundle.putInt("index", indx);
                bundle.putBoolean("nuevo",true);
                intent.putExtras(bundle);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        cargarContactos();
    } //Fin onCreate

    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s)
            {
                Buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void Buscar (String s)
    {
        ArrayList<Contacto> listaBuscar = new ArrayList<>();

        for (int x=0;x<filtro.size();x++)
        {
            if (filtro.get(x).getNombre().toUpperCase().contains(s.toUpperCase()))
            {
                listaBuscar.add(filtro.get(x));
            }
            contactos = listaBuscar;
            tblLista.removeAllViews();
            cargarContactos();
        }
    }

    public void cargarContactos() {
        for (int x=0; x < contactos.size(); x++)
        {
            TableRow nRow = new TableRow(list_activity.this);
            Contacto c = new Contacto();
            TextView nText = new TextView(list_activity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(list_activity.this);
            Button btnBorrar = new Button(list_activity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    com.example.prctica1c2.Contacto c = (com.example.prctica1c2.Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", c);
                    oBundle.putInt("index", Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });
            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int y =0; y < filtro.size();y++)
                    {
                        if (filtro.get(y).getId() == (int) c.getId())
                        {
                            filtro.remove(filtro.get(y));
                            contactos=filtro;
                            break;
                        }
                        tblLista.removeAllViews();
                        cargarContactos();
                    }
                }
            });
            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index,x);
            btnBorrar.setTag(R.string.contacto_g, c);
            btnBorrar.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            nRow.addView(btnBorrar);
            tblLista.addView(nRow);
        }
    }
}
